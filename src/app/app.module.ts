import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

import  { HttpClientModule } from '@angular/common/http';
import { PostsModule } from './pages/posts/posts.module';
import { HomeModule } from './pages/home/home.module';
import { RouterModule } from '@angular/router';
import { PhotosModule } from './pages/photos/photos.module';
import { UsersModule } from './pages/users/users.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    SharedModule,
    PostsModule,
    HomeModule,
    PhotosModule,
    UsersModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
