import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PostsService } from 'src/app/core/services/posts/posts.service';

@Component({
  selector: 'app-posts-detail',
  templateUrl: './posts-detail.component.html',
  styleUrls: ['./posts-detail.component.scss']
})
export class PostsDetailComponent implements OnInit {
  post: any;
  getPostSubscribe: Subscription = new Subscription();

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getPost();
  }

  getPost() {
    const id = this.route.snapshot.paramMap.get('id');
    this.getPostSubscribe = this.postsService.getPostsById(id).subscribe(resp => {
      this.post = resp;
    });
  }

  ngOnDestroy(): void {
    this.getPostSubscribe.unsubscribe();
  }
}
