import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PhotosService } from '../../core/services/photos/photos.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  photos: any;
  getPhotosSubscribe: Subscription = new Subscription();

  constructor(private photosService: PhotosService) {}

  ngOnInit(): void {
    this.getPhotos();
  }

  getPhotos() {
    this.getPhotosSubscribe = this.photosService.getPhotos().subscribe(resp => {
      this.photos = resp;
    })
  }

  ngOnDestroy(): void {
    this.getPhotosSubscribe.unsubscribe();
  }
}
