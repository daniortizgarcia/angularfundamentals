import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTitle]'
})
export class TitleDirective {

  constructor(elem: ElementRef, render: Renderer2) {
    console.log('Soy title');
    render.setStyle(elem.nativeElement, 'border', '1px solid black')
  }

}
