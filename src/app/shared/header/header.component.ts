import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title: string;
  @Output() recibePadre = new EventEmitter<any>();

  constructor() {}
  ngOnInit(): void {}

  onSubmit() {
    this.recibePadre.emit('Esto es una prueba del hijo al padre header');
  }
}
