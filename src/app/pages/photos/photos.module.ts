import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotosComponent } from './photos.component';
import { PhotosDetailComponent } from './photos-detail/photos-detail.component';
import { RouterModule } from '@angular/router';
import { PhotosRoutingModule } from './photos-routing.module';

@NgModule({
  declarations: [PhotosComponent, PhotosDetailComponent],
  imports: [
    CommonModule,
    RouterModule,
    PhotosRoutingModule
  ],
  exports: [PhotosComponent, PhotosDetailComponent],
})
export class PhotosModule { }
