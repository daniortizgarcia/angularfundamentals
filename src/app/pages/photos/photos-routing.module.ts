import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhotosDetailComponent } from './photos-detail/photos-detail.component';
import { PhotosComponent } from './photos.component';

const routes: Routes = [
  {
    path: '',
    component: PhotosComponent,
    pathMatch: 'full',
  },
  {
    path: 'photos/:id',
    component: PhotosDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotosRoutingModule { }
