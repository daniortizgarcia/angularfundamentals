import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http:HttpClient) {}

  getUsers(): Observable<any> {
    return this.http.get<any>(environment.api + 'users');
  }
  getUsersById(id): Observable<any> {
    return this.http.get<any>(environment.api + 'users/' + id);
  }

  removeUsers(id): Observable<any> {
    return this.http.delete<any>(environment.api + 'users/' + id);
  }
}
