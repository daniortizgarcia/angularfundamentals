import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss']
})
export class UsersFormComponent implements OnInit {
  public formUser = this.formBuilder.group({
    registeredOn: new Date(),
    name:[ ''.toLowerCase(), [Validators.required]],
    email: ['', [ Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  });
  myName = 'Pedro';

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
  }

  submitForm() {
    console.log(this.formUser);
  }

}
