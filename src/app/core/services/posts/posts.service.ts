import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http:HttpClient) {}

  getPosts(): Observable<any> {
    return this.http.get<any>(environment.api + 'posts');
  }

  addPosts(): Observable<any> {
    return this.http.post<any>(environment.api + 'posts', {});
  }

  updatePosts(): Observable<any> {
    return this.http.put<any>(environment.api + 'posts', {});
  }

  getPostsById(id): Observable<any> {
    return this.http.get<any>(environment.api + 'posts/' + id);
  }

  removePosts(id): Observable<any> {
    return this.http.delete<any>(environment.api + 'posts/' + id);
  }
}
