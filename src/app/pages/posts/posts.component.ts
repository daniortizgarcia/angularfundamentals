import { Component, OnInit } from '@angular/core';
import { Subscriber, Subscription } from 'rxjs';
import { PostsService } from 'src/app/core/services/posts/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: any;
  getPostsSubscribe: Subscription = new Subscription();
  removePostSubscribe: Subscription = new Subscription();

  constructor(
    private postsService: PostsService
  ) {}

  ngOnInit(): void {
    this.getPosts();
  }

  getPosts():void {
    this.getPostsSubscribe = this.postsService.getPosts().subscribe(resp => {
      this.posts = resp;
    });
  }

  removePost(postId):void {
    this.removePostSubscribe = this.postsService.removePosts(postId).subscribe(resp => {
      console.log(resp);
    })
  }

  ngOnDestroy(): void {
    this.getPostsSubscribe.unsubscribe();
    this.removePostSubscribe.unsubscribe();
  }
}
