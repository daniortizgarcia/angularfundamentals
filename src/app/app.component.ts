import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  titleHeader: string;
  titleFooter: string;
  mensaje: string;

  constructor() {
    this.titleHeader = 'Header';
    this.titleFooter = 'Footer';
  }

  reciboHijo(msg) {
    this.mensaje = msg;
  }
}
