import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PhotosService } from 'src/app/core/services/photos/photos.service';

@Component({
  selector: 'app-photos-detail',
  templateUrl: './photos-detail.component.html',
  styleUrls: ['./photos-detail.component.scss']
})
export class PhotosDetailComponent implements OnInit {
  photo: any;
  getPhotosSubscribre: Subscription = new Subscription();

  constructor(private photosService: PhotosService, private router: ActivatedRoute) {}

  ngOnInit(): void {
    this.getPhotos();
  }

  getPhotos() {
    const id = this.router.snapshot.paramMap.get('id');
    this.getPhotosSubscribre = this.photosService.getPhotosById(id).subscribe(resp => {
      this.photo = resp;
    });
  }

  ngOnDestroy(): void {
    this.getPhotosSubscribre.unsubscribe();
  }
}
