import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UsersService } from 'src/app/core/services/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any;
  getUsersSubscribe: Subscription = new Subscription();
  removeUserSubscribe: Subscription = new Subscription();

  constructor(
    private usersService: UsersService
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers():void {
    this.getUsersSubscribe = this.usersService.getUsers().subscribe(resp => {
      this.users = resp;
    });
  }

  removeUsers(userId):void {
    this.removeUserSubscribe = this.usersService.removeUsers(userId).subscribe(resp => {
      console.log(resp);
    })
  }

  ngOnDestroy(): void {
    this.getUsersSubscribe.unsubscribe();
    this.removeUserSubscribe.unsubscribe();
  }
}
