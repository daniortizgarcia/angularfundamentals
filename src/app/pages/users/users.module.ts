import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import { UsersRoutingModule } from './users-routing.module';
import { UsersFormComponent } from './users-form/users-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {CalendarModule} from 'primeng/calendar';
import {PanelModule} from 'primeng/panel';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [UsersComponent, UsersDetailComponent, UsersFormComponent],
  imports: [
    CommonModule,
    RouterModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    InputTextModule,
    PasswordModule,
    CalendarModule,
    PanelModule,
    ButtonModule
  ],
  exports: [UsersComponent, UsersDetailComponent, UsersFormComponent]
})
export class UsersModule { }
