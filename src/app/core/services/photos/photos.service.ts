import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http:HttpClient) {}

  getPhotos(): Observable<any> {
    return this.http.get<any>(environment.api + 'photos');
  }
  getPhotosById(id): Observable<any> {
    return this.http.get<any>(environment.api + 'photos/' + id);
  }
}
