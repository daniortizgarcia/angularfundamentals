import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PostsComponent } from './posts.component';
import { PostsDetailComponent } from './posts-detail/posts-detail.component';
import { PostsRoutingModule } from './posts-routing.module';

@NgModule({
  declarations: [PostsComponent, PostsDetailComponent],
  imports: [
    CommonModule,
    RouterModule,
    PostsRoutingModule
  ],
  exports: [PostsComponent, PostsDetailComponent]
})
export class PostsModule { }
