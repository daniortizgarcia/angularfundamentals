import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() title: string;
  @Output() recibePadre = new EventEmitter<any>();

  constructor() {}
  ngOnInit(): void {}

  onSubmit() {
    this.recibePadre.emit('Esto es una prueba del hijo al padre footer');
  }
}
