import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { ReverseStringPipe } from './pipes/reverse-string/reverse-string.pipe';
import { TitleDirective } from './directives/title.directive';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ReverseStringPipe,
    TitleDirective
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ReverseStringPipe,
    TitleDirective
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SharedModule {}
